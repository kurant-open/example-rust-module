use numpy::{PyReadonlyArray1};
use pyo3::prelude::{pymodule, PyModule, PyResult, Python, pyfunction, wrap_pyfunction};
//use pyo3::{PyErr, PyObject, Py, exceptions};
use pyo3::*;
use pyo3::types::*;


/// Takes dot product of two numpy vectors (1D arrays)
#[pyfunction]
fn dot_product(a: PyReadonlyArray1<f32>, b: PyReadonlyArray1<f32>) -> PyResult<f32> {
    if (a.len() != b.len()) {
        return Err(PyErr::new::<exceptions::PyValueError, _>("Arrays have to be of the same size"));
    }
    let mut retval = 0f32;
    let a_view = a.as_array();
    let b_view = b.as_array();

    for i in 0..a.len() {
        retval += a_view[i] * b_view[i];
    }
    Ok(retval)
}

/// Takes a number from python and modifies it in-place
#[pyfunction]
fn get_attribute(py: Python<'_>, obj: PyObject, attr: String) -> PyResult<PyObject> {
    let locked_obj = obj.as_ref(py);
    let value = locked_obj.getattr(attr)?;
    Ok(value.to_object(py))
}

/// Creates a module and adds functions to it
#[pymodule]
fn example_rust_module(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(dot_product, m)?)?;
    m.add_function(wrap_pyfunction!(get_attribute, m)?)?;
    Ok(())
}
