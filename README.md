# PyO3 example module
A simple example project to demonstrate how create a rust project with python bindings using PyO3.
## Creating a project
Create a project folder, and run `maturin new` to quickly bootstrap the project. `maturin` is available on pypi. This creates the `Cargo.toml` and `pyproject.toml` files defining some basic project metadata and defines the build system.
To be able to use standard installation procedure to build and install the project (alternative is using `maturin install`), add a `setup.py` and add the rust project as an extension using `setuptools_rust`, like so:
``` python
from setuptools_rust import Binding, RustExtension

setup(
    ...
    rust_extensions=[RustExtension("[module path]", binding=Binding.PyO3)],
		zip_safe=False,
		...
)
```
Note that the rust extension cannot run from a compressed install, thus `zip_safe=False`.
You also have to replace the `[build-system]` in `pyproject.toml` with
``` toml
[build-system]
requires = ["setuptools", "setuptools_rust", "wheel"]
```
Installing the project can now be done with pip and similar like normal.
