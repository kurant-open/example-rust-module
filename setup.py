from setuptools import setup
from setuptools_rust import Binding, RustExtension

setup(
    name="example_rust_module",
    version="1.0",
    rust_extensions=[RustExtension("example_rust_module", binding=Binding.PyO3)],
    install_requires=[
        "numpy"
    ],
    zip_safe=False,
)
