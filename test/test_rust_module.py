import example_rust_module as erm
import numpy as np
import pytest

@pytest.fixture
def test_object():
    class Foo:
        def __init__(self):
            self.bar = 123
    return Foo()


def test_dot_product():
    a = np.array([1, 2, 3], dtype=np.float32)
    b = np.array([4, 5, 6], dtype=np.float32)
    rust_result = erm.dot_product(a, b)
    numpy_result = np.dot(a, b)

    assert np.isclose(rust_result, numpy_result)

def test_attribute_getter(test_object):
    assert erm.get_attribute(test_object, "bar") == test_object.bar

def test_attribute_getter_missing_attribute(test_object):
    with pytest.raises(AttributeError):
        erm.get_attribute(test_object, "missing_attribute")
